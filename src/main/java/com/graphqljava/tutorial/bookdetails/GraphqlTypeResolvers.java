package com.graphqljava.tutorial.bookdetails;

import com.graphqljava.tutorial.bookdetails.vo.BookDetailVO;
import graphql.schema.TypeResolver;
import org.springframework.stereotype.Component;

@Component
public class GraphqlTypeResolvers {


    public TypeResolver getBaseBookTypeResolver() {
        return typeResolutionEnvironment -> {
            Object objectType = typeResolutionEnvironment.getObject();
            return null;
        };
    }

    public TypeResolver getBaseAuthorResolver() {
        return typeResolutionEnvironment -> {
            Object objectType = typeResolutionEnvironment.getObject();
            return null;
        };
    }

    public TypeResolver getBookDetailResolver() {
        return typeResolutionEnvironment -> {
            Object objectType = typeResolutionEnvironment.getObject();
            if(objectType instanceof BookDetailVO.BookDetailVOBuilder) {
                return typeResolutionEnvironment.getSchema().getObjectType("BookDetail");
            }
            return null;
        };
    }
}
