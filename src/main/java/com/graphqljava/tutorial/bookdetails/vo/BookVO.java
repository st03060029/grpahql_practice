package com.graphqljava.tutorial.bookdetails.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BookVO {

    private String id;
    private String name;
    private Integer pageCount;
    private BookDetailVO bookDetailVO;
    private AuthorVO author;
    private String status;

}
