package com.graphqljava.tutorial.bookdetails.vo;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AuthorVO {

    private String id;
    private String firstName;
    private String lastName;

}
