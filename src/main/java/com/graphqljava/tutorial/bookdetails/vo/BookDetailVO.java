package com.graphqljava.tutorial.bookdetails.vo;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BookDetailVO {

    private Integer pageCount;

    private String size;

}
