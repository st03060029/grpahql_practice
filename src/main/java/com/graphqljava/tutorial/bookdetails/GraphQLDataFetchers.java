package com.graphqljava.tutorial.bookdetails;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableMap;
import com.graphqljava.tutorial.bookdetails.vo.BookDetailVO;
import com.graphqljava.tutorial.bookdetails.vo.BookVO;
import graphql.schema.DataFetcher;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class GraphQLDataFetchers {

    private static List<Map<String, Object>> booksRawData = Arrays.asList(
            ImmutableMap.of("id", "book-1",
                    "name", "Harry Potter and the Philosopher's Stone",
                    "bookDetail", BookDetailVO.builder().pageCount(1234).size("B5"),
                    "authorId", "author-1",
                    "releaseDate", "1609430400"),
            ImmutableMap.of("id", "book-2",
                    "name", "Moby Dick",
                    "bookDetail", BookDetailVO.builder().pageCount(635).size("B5"),
                    "authorId", "author-2",
                    "releaseDate", "1612108800"),
            ImmutableMap.of("id", "book-3",
                    "name", "Interview with the vampire",
                    "bookDetail", BookDetailVO.builder().pageCount(371).size("A4"),
                    "authorId", "author-3",
                    "releaseDate", "1614528000"),
            ImmutableMap.of("id", "book-4",
                    "name", "The book with no author",
                    "bookDetail", BookDetailVO.builder().pageCount(1).size("A5"),
                    "authorId", "",
                    "releaseDate", "1617206400"),
            ImmutableMap.of("id", "book-5",
                    "name", "The book with Author-4",
                    "bookDetail", BookDetailVO.builder().pageCount(1).size("B5"),
                    "authorId", "author-4",
                    "releaseDate", "1619798400")
    );

    private static List<Map<String, String>> authorsRawData = Arrays.asList(
            ImmutableMap.of("id", "author-1",
                    "firstName", "Joanne",
                    "lastName", "Rowling"),
            ImmutableMap.of("id", "author-2",
                    "firstName", "Herman",
                    "lastName", "Melville"),
            ImmutableMap.of("id", "author-3",
                    "firstName", "Anne",
                    "lastName", "Rice"),
            ImmutableMap.of("id", "author-4",
                    "firstName", "HAHA",
                    "lastName", "Riceee")
    );

    public DataFetcher getBookByIdDataFetcher() {
        return dataFetchingEnvironment -> {
            String bookId = dataFetchingEnvironment.getArgument("id");
            return booksRawData
                    .stream()
                    .filter(book -> book.get("id").equals(bookId))
                    .findFirst()
                    .orElse(null);
        };
    }

    public DataFetcher getAllBookDataFetcher() {
        return dataFetchingEnvironment -> booksRawData
                .stream()
                .filter(book -> book.get("id") != null)
                .collect(Collectors.toList());
    }

    public DataFetcher getAuthorDataFetcher() {
        return dataFetchingEnvironment -> {
            Map<String,String> book = dataFetchingEnvironment.getSource();
            String authorId = book.get("authorId");
            return authorsRawData
                    .stream()
                    .filter(author -> author.get("id").equals(authorId))
                    .findFirst()
                    .orElse(null);
        };
    }

    public DataFetcher getPageCountDataFetcher() {
        return dataFetchingEnvironment -> {
            Map<String,String> book = dataFetchingEnvironment.getSource();
            return book.get("pageCount");
        };
    }

    public DataFetcher getBooksByFuzzySearchAuthorName() {

        return dataFetchingEnvironment -> {
            String keyword = dataFetchingEnvironment.getArgument("keyword");
            List<Map<String, String>> authors = authorsRawData.stream()
                    .filter(f -> (f.get("firstName").contains(keyword) || f.get("lastName").contains(keyword)))
                    .collect(Collectors.toList());
            List<String> authorsId = new ArrayList<>();

            for(Map<String, String> author : authors) {
                authorsId.add(author.get("id"));
            }

            List<Map<String, Object>> booksOfAuthor = new ArrayList<>();
            for(String authorId : authorsId) {
                booksOfAuthor.addAll(booksRawData.stream().filter(f -> f.get("authorId").equals(authorId)).collect(Collectors.toList()));
            }
            return booksOfAuthor;
        };
    }

    public DataFetcher getAddBookDataFetcher() {
        return dataFetchingEnvironment -> {
            Map<String, Object> input = dataFetchingEnvironment.getArgument("book");

            ObjectMapper objectMapper = new ObjectMapper();
            BookVO book = objectMapper.convertValue(input, BookVO.class);
            System.out.println(book);
            book.setId("123");
            book.setStatus("OK");
            book.getAuthor().setId("0123456789");

            return book;
        };
    }
}
